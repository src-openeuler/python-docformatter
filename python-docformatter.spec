%global _empty_manifest_terminate_build 0
%global pypi_name docformatter

Name:           python-%{pypi_name}
Version:        1.7.5
Release:        1
Summary:        Docformatter automatically formats docstrings to follow a subset of the PEP 257 conventions.It also handles some of the PEP 8 conventions.

License:        MIT
URL:            https://files.pythonhosted.org/packages/f4/44/aba2c40cf796121b35835ea8c00bc5d93f2f70730eca53b36b8bbbfaefe1/docformatter-1.7.5.tar.gz
Source0:        %{url}/archive/%{version}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:	python3-hatchling
BuildRequires:	python3-virtualenv
BuildRequires:  python3-poetry-core

%description
Docformatter automatically formats docstrings to follow a subset of the PEP 257 conventions.It also handles some of the PEP 8 conventions.


%package -n     python3-%{pypi_name}
Summary:        %{summary}

%description -n python3-%{pypi_name}
Docformatter automatically formats docstrings to follow a subset of the PEP 257 conventions.It also handles some of the PEP 8 conventions.


%prep
%autosetup -p1 -n %{pypi_name}-%{version}

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-%{pypi_name}
%doc README.rst
%license LICENSE
%{python3_sitelib}/docformatter*.dist-info/
%{python3_sitelib}/docformatter
%{python3_sitelib}/LICENSE
%{_bindir}/docformatter

%changelog
* Tue Aug 01 2023 zhangchenglin <zhangchenglin@kylinos.cn> - 1.7.5-1
- Update package to version 1.7.5

* Tues Jun 27 2023 zoujiancang<jczou@isoftstone.com> - 1.7.3-1
- Initial package
